import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  nombre = 'David';
  nombre2 =  'andres daVid velAndia suÁrez';
  arreglo = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  PI = Math.PI;
  percentNumber = 0.234;
  salario = 12345.6;
  heroe = {
    nombre: 'Logan',
    nick: 'Wolverine',
    edad: 500,
    direccion: {
      calle: 'Primera',
      casa: 19
    }
  };
  valorDePromesa = new Promise( (resolve, reject) => {
    setTimeout(() => resolve('Llegó la data'), 3500);
  });

  fecha = new Date();

  video = 'LAn5Oka1QiI';

  activar = false;

  activarContrasena() {
    this.activar = !this.activar;
  }
}
