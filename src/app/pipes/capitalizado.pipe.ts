import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'capitalizado' })
export class CapitalizadoPipe implements PipeTransform {
    // ...args: any[]
    // arg1, arg2, arg3, arg4
  transform(value: string, todas: boolean = true): string {
    value = value.toLocaleLowerCase();
    const nombres = value.split(' ');
    if (todas) {
        for (let i = 0; i < nombres.length; i++) {
            const nombre = nombres[i];
            nombres[i] = nombre[0].toLocaleUpperCase() + nombre.substr(1);
        }
    } else {
        nombres[0] = nombres[0][0].toLocaleUpperCase() + nombres[0].substr(1);
    }
    return nombres.join(' ');
  }
}
