import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'contrasena'
})
export class ContrasenaPipe implements PipeTransform {

  transform(value: string, activar: boolean = false): any {
    if (activar) {
      return Array(value.length + 1).join('*');
    } else {
      return value;
    }
  }

}
